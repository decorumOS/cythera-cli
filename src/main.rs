
extern crate clap;
extern crate yaml_rust;
extern crate sqlite;

use std::env;
use clap::{Arg, App, SubCommand};
use yaml_rust::{YamlLoader, YamlEmitter};

pub mod opts;       /* handle parsed arguments */
pub mod db;         /* database interfacing */
pub mod notify;     /* errors, warnings, and notifies */




fn check_root() -> bool {
    /* check that Cythera is running as root. for now, return false. */
    return false;
}


fn check_lock() -> bool {
    /*
    check for the existence of the Cythera lock file. used to signify if something
    caused Cythera to crash whilst it was previously doing something. maybe this
    lock file could contain information about what cythera was doing and this
    could be used for bug reporting in the future. for now, there is no lock
    file implemented, so just return false; there is no lock file.
    */
    return false;
}


fn unlock() {
    let lock_status = check_lock();
}





/* Main
------------------------------------------------------------------------------------*/

fn main() {
    /*
    Cythera mode:
    0 = undefined or invalid
    1 = config
    2 = search
    */
    let mut mode = 0;

    /* Check Cythera is running as root */
    check_root();

    /* check for the lock file hash to determine a new/damaged install */
    unlock();

    /* Get opts */
    let opts = opts::get_opts();


}


/*

install packages to /pkg/arbituary-package-identifier instead of /usr,
then create links from /usr to /pkg/the-package-binary-you-want-to-run
this allows users to have multiple versions of the same applications installed
but only activly use one. it also will make tracking installed packages much easier.

This also allows us to easily see if installing a package may cause conflicts; if it 
is replacing software links that are already installed.
*/