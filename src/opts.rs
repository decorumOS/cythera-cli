extern crate clap;
use clap::{Arg, App, SubCommand};



/* CLI Paramerters
------------------------------------------------------------------------------------*/

pub fn get_opts() -> clap::ArgMatches<'static> {
    let matches = App::new("cythera-cli")
                            .version("0.1.0")
                            .author("p01ar <polar@ever3st.com>")
                            .about("Cythera Package Manager.")
                            .arg(Arg::with_name("verbose")
                                .short("v")
                                .long("verbose")
                                .help("Enable verbose output")
                            )


                            .subcommand(SubCommand::with_name("config")
                            /* Used to search cythera databases & caches */
                                .about("Configure cythera attributes.")
                                .arg(Arg::with_name("cacheless")
                                    .short("c")
                                    .long("cacheless")
                                    .help("enable/disable the local package cache.")
                                    /* 
                                    maybe ask if the user wants to fetch or remove
                                    packages if this option is toggled?
                                    */
                                )
                                .arg(Arg::with_name("rebuild")
                                    .short("R")
                                    .help("revert Cythera to default settings.")
                                )
                                /*
                                possible commands:
                                ??????????????????????????????????
                                */
                            )


                            .subcommand(SubCommand::with_name("query")
                            /* Used to search cythera databases & caches */
                                .about("Search installed Cythera repositories.")
                                .arg(Arg::with_name("pkg_name")
                                    .short("n")
                                    .long("pkg-name")
                                    .help("Search package(s) by package name.")
                                )
                                .arg(Arg::with_name("pkg_family")
                                    .short("f")
                                    .long("pkg-family")
                                    .help("Search package(s) by package family.")
                                )
                                .arg(Arg::with_name("catalogue")
                                    .short("c")
                                    .long("list-repo")
                                    .help("List installed Cythera packages.")
                                )
                                .arg(Arg::with_name("certs")
                                    .short("k")
                                    .long("list-cert")
                                    .help("List installed Cythera certification servers.")
                                )
                                .arg(Arg::with_name("repo")
                                    .short("j")
                                    .long("repo")
                                    .help("List installed Cythera package repositories.")
                                )
                                /*
                                possible commands:
                                local -- only search locally stored packages
                                list-deps -- list the dependancies of a specified
                                package
                                */
                            )


                            .subcommand(SubCommand::with_name("install")
                            /* Used to install new packages */
                                .about("Install Cythera package(s).")
                                .arg(Arg::with_name("install-force")
                                    .short("y")
                                    .long("force")
                                    .help("Enable verbose output")
                                )

                                .arg(Arg::with_name("pkg")
                                    .short("p")
                                    .help("print debug information verbosely.")
                                )
                                /*
                                possible commands:
                                canary -- install the untested development
                                version of the package. if the installed package
                                repos cannot find a canary package, notify user
                                and ask if release version should be installed.
                                naitve -- install to /usr instead of /pkg
                                */
                            )


                            .subcommand(SubCommand::with_name("remove")
                            /* Used to remove installed packages */
                                .about("Remove Cythera package(s).")
                                .arg(Arg::with_name("install-force")
                                    .short("y")
                                    .long("force")
                                    .help("Enable verbose output")
                                )

                                .arg(Arg::with_name("pkg")
                                    .short("p")
                                    .help("print debug information verbosely.")
                                )
                                /*
                                possible commands:
                                ???????????????????
                                */
                            )


                            .subcommand(SubCommand::with_name("repair")
                            /* Used to repair cythera attributes*/
                                .about("Repair Cythera attirbutes")
                                .arg(Arg::with_name("pkg-cache")
                                    .short("C")
                                    .help("Rebuild the package cache")
                                )
                                .arg(Arg::with_name("pkg-db")
                                    .short("B")
                                    .help("Attepmt to rebuild the installed package database.")
                                )
                                .arg(Arg::with_name("sweep-ln")
                                    .short("l")
                                    .help("Attepmt to repair package links in /usr.")
                                )
                                /*
                                possible commands:
                                sweep-cache -- clean out from the package cache
                                packages that are no  longer installed
                                or used by installed programs  
                                */
                            )


    .get_matches();

    if let Some(matches) = matches.subcommand_matches("config") {
        if matches.is_present("cacheless") {
            println!("cacheless...");
        } else {
            println!("Printing normally...");
        }
    }

    return matches;
}

fn set_params() {

}